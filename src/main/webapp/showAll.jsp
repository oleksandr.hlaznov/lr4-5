<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.List, glaznyov.lr45.model.UserRegistration" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Регистрации пользователей</title>
    <link rel="stylesheet" href="static/css/styles.css">
</head>
<body>
<div class="background"></div>
<div class="content">
    <h1>Регистрации пользователей</h1>
    <a href="context.xml" class="btn">Главная</a>
    <a href="UserRegistrationController?action=insert" class="btn">Добавить новую регистрацию</a>
    <table class="table">
        <tr>
            <th>ID</th>
            <th>Дата регистрации</th>
            <th>Логин</th>
            <th>Фамилия</th>
            <th>Имя</th>
            <th>Email</th>
            <th>ICQ</th>
            <th>Пол</th>
            <th>Подписка на новости</th>
            <th>Действия</th>
        </tr>
        <%
            List<UserRegistration> userRegistrations = (List<UserRegistration>) request.getAttribute("userRegistrations");
            for (UserRegistration userRegistration : userRegistrations) {
        %>
        <tr>
            <td><%= userRegistration.getId() %></td>
            <td><%= userRegistration.getRegistrationDate() %></td>
            <td><%= userRegistration.getLogin() %></td>
            <td><%= userRegistration.getLastName() %></td>
            <td><%= userRegistration.getFirstName() %></td>
            <td><%= userRegistration.getEmail() %></td>
            <td><%= userRegistration.getIcq() %></td>
            <td><%= userRegistration.getGender() %></td>
            <td><%= userRegistration.getNewsletterSubscription() ? "Да" : "Нет" %></td>
            <td>
                <a href="UserRegistrationController?action=edit&userRegistrationId=<%= userRegistration.getId() %>">Редактировать</a>
                <a href="UserRegistrationController?action=delete&userRegistrationId=<%= userRegistration.getId() %>">Удалить</a>
            </td>
        </tr>
        <% } %>
    </table>
    <a href="UserRegistrationController?action=insert" class="btn">Добавить новую регистрацию</a>
</div>
</body>
</html>
