<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    String errorMessage = (String) request.getAttribute("errorMessage");
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Добавить/изменить регистрацию пользователя</title>
    <link rel="stylesheet" href="static/css/styles.css">
</head>
<body>
<div class="background"></div>
<div class="content">
    <h1>Добавить/изменить регистрацию пользователя</h1>
    <% if (errorMessage != null) { %>
    <div class="error-message"><%= errorMessage %></div>
    <% } %>
    <a href="context.xml" class="btn">Главная</a>
    <a href="UserRegistrationController?action=showAll" class="btn">Просмотреть всех пользователей</a>
    <form action="UserRegistrationController" method="post">
        <input type="hidden" name="userRegistrationId" value="${UserRegistration.id}">
        <label for="registrationDate">Дата регистрации:</label>
        <input type="text" id="registrationDate" name="registrationDate" value="${UserRegistration.registrationDate}">
        <label for="login">Логин:</label>
        <input type="text" id="login" name="login" value="${UserRegistration.login}">
        <label for="password">Пароль:</label>
        <input type="password" id="password" name="password" value="${UserRegistration.password}">
        <label for="passwordConfirmation">Подтверждение пароля:</label>
        <input type="password" id="passwordConfirmation" name="passwordConfirmation" value="${UserRegistration.passwordConfirmation}">
        <label for="lastName">Фамилия:</label>
        <input type="text" id="lastName" name="lastName" value="${UserRegistration.lastName}">
        <label for="firstName">Имя:</label>
        <input type="text" id="firstName" name="firstName" value="${UserRegistration.firstName}">
        <label for="email">Email:</label>
        <input type="email" id="email" name="email" value="${UserRegistration.email}">
        <label for="icq">ICQ:</label>
        <input type="text" id="icq" name="icq" value="${UserRegistration.icq}">
        <label for="gender">Пол:</label>
        <input type="text" id="gender" name="gender" value="${UserRegistration.gender}">
        <label for="newsletterSubscription">Подписка на новостную рассылку:</label>
        <input type="checkbox" id="newsletterSubscription" name="newsletterSubscription" ${UserRegistration.newsletterSubscription ? 'checked' : ''}>
        <button type="submit" class="btn">Сохранить</button>
    </form>
</div>
</body>
</html>
