package glaznyov.lr45;

import glaznyov.lr45.model.UserRegistration;
import glaznyov.lr45.service.UserRegistrationService;

import java.util.List;

public class App {

    public static void main(String[] args) {
        UserRegistrationService userRegistrationService = new UserRegistrationService();

        List<UserRegistration> userRegistrationList = userRegistrationService.findAll();
        // Вывод всех записей из базы данных
        System.out.println("Все регистрации пользователей:");
        for (var user : userRegistrationList) {
            System.out.println(user);
        }

        var userRegistration = userRegistrationService.findById(6);
        System.out.println(userRegistration);
    }
}
