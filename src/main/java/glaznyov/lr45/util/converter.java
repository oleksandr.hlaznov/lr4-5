package glaznyov.lr45.util;

import glaznyov.lr45.model.UserRegistration;
import glaznyov.lr45.model.UserRegistrationModel;

public class converter {
    public static void userRegistrationModelToEntity(UserRegistration userRegistration, UserRegistrationModel model ) {
        userRegistration.setRegistrationDate(model.getRegistrationDate());
        userRegistration.setLogin(model.getLogin());
        userRegistration.setPassword(model.getPassword());
        userRegistration.setPasswordConfirmation(model.getPasswordConfirmation());
        userRegistration.setLastName(model.getLastName());
        userRegistration.setFirstName(model.getFirstName());
        userRegistration.setEmail(model.getEmail());
        userRegistration.setIcq(model.getIcq());
        userRegistration.setGender(model.getGender());
        userRegistration.setNewsletterSubscription(model.getNewsletterSubscription());
    }
}
