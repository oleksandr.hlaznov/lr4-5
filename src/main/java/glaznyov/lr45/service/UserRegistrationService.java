package glaznyov.lr45.service;

import glaznyov.lr45.dao.UserRegistrationDAO;
import glaznyov.lr45.model.UserRegistration;

import java.util.List;

public class UserRegistrationService {
    private static UserRegistrationDAO userRegistrationDAO;

    public UserRegistrationService() {
        userRegistrationDAO = new UserRegistrationDAO();
    }

    public void persist(UserRegistration entity) {
        userRegistrationDAO.openCurrentSessionWithTransaction();
        userRegistrationDAO.persist(entity);
        userRegistrationDAO.closeCurrentSessionWithTransaction();
    }

    public void update(UserRegistration entity) {
        userRegistrationDAO.openCurrentSessionWithTransaction();
        userRegistrationDAO.update(entity);
        userRegistrationDAO.closeCurrentSessionWithTransaction();
    }

    public UserRegistration findById(Integer id) {
        userRegistrationDAO.openCurrentSession();
        UserRegistration userRegistration = userRegistrationDAO.findById(id);
        userRegistrationDAO.closeCurrentSession();
        return userRegistration;
    }

    public void delete(Integer id) {
        userRegistrationDAO.openCurrentSessionWithTransaction();
        UserRegistration userRegistration = userRegistrationDAO.findById(id);
        userRegistrationDAO.delete(userRegistration);
        userRegistrationDAO.closeCurrentSessionWithTransaction();
    }

    public List<UserRegistration> findAll() {
        userRegistrationDAO.openCurrentSession();
        List<UserRegistration> userRegistrationList = userRegistrationDAO.findAll();
        userRegistrationDAO.closeCurrentSession();
        return userRegistrationList;
    }

    public void deleteAll() {
        userRegistrationDAO.openCurrentSessionWithTransaction();
        userRegistrationDAO.deleteAll();
        userRegistrationDAO.closeCurrentSessionWithTransaction();
    }

    public UserRegistrationDAO userRegistrationDAO() {
        return userRegistrationDAO;
    }
}
