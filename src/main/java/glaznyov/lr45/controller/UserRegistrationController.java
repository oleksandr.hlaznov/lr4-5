package glaznyov.lr45.controller;

import glaznyov.lr45.util.converter;
import glaznyov.lr45.model.UserRegistration;
import glaznyov.lr45.model.UserRegistrationModel;
import glaznyov.lr45.service.UserRegistrationService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/UserRegistrationController")
public class UserRegistrationController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final String INSERT_OR_EDIT = "/insert.jsp";
    private static final String LIST_USER = "/showAll.jsp";
    private final UserRegistrationService userRegistrationService;

    public UserRegistrationController() {
        super();
        userRegistrationService = new UserRegistrationService();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String forward = "";
        String action = request.getParameter("action");
        List<UserRegistration> userRegistrationsList = new ArrayList<>();

        if (action.equalsIgnoreCase("delete")) {
            int userRegistrationId = Integer.parseInt(request.getParameter("userRegistrationId"));
            userRegistrationService.delete(userRegistrationId);
            forward = LIST_USER;
            userRegistrationsList = userRegistrationService.findAll();
            request.setAttribute("userRegistrations", userRegistrationsList);
        } else if (action.equalsIgnoreCase("edit")) {
            forward = INSERT_OR_EDIT;
            int userRegistrationId = Integer.parseInt(request.getParameter("userRegistrationId"));
            UserRegistration userRegistration = userRegistrationService.findById(userRegistrationId);
            request.setAttribute("UserRegistration", userRegistration);
        } else if (action.equalsIgnoreCase("showAll")) {
            forward = LIST_USER;
            userRegistrationsList = userRegistrationService.findAll();
            request.setAttribute("userRegistrations", userRegistrationsList);
        } else {
            forward = INSERT_OR_EDIT;
            request.setAttribute("userRegistrations", userRegistrationsList);
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        UserRegistrationModel userModel = new UserRegistrationModel();
        userModel.setRegistrationDate(request.getParameter("registrationDate"));
        userModel.setLogin(request.getParameter("login"));
        userModel.setPassword(request.getParameter("password"));
        userModel.setPasswordConfirmation(request.getParameter("passwordConfirmation"));
        userModel.setLastName(request.getParameter("lastName"));
        userModel.setFirstName(request.getParameter("firstName"));
        userModel.setEmail(request.getParameter("email"));
        userModel.setIcq(request.getParameter("icq"));
        userModel.setGender(request.getParameter("gender"));
        userModel.setNewsletterSubscription(Boolean.valueOf(request.getParameter("newsletterSubscription")));

        // Check for required fields
        if (userModel.getRegistrationDate() == null || userModel.getRegistrationDate().isEmpty()
                || userModel.getLastName() == null || userModel.getLastName().isEmpty()
                || userModel.getFirstName() == null || userModel.getFirstName().isEmpty()
                || userModel.getLogin() == null || userModel.getLogin().isEmpty()
                || userModel.getPassword() == null || userModel.getPassword().isEmpty()
                || userModel.getPasswordConfirmation() == null || userModel.getPasswordConfirmation().isEmpty()
                || userModel.getEmail() == null || userModel.getEmail().isEmpty()) {

            request.setAttribute("errorMessage", "Please fill in all required fields.");
            RequestDispatcher view = request.getRequestDispatcher(INSERT_OR_EDIT);
            view.forward(request, response);
            return;
        }

        // If validation passes, continue processing
        String userId = request.getParameter("userRegistrationId");
        if (userId == null || userId.isEmpty()) {
            UserRegistration user = new UserRegistration();
            converter.userRegistrationModelToEntity(user, userModel);
            userRegistrationService.persist(user);
        } else {
            userModel.setId(Integer.parseInt(userId));
            UserRegistration user = userRegistrationService.findById(userModel.getId());
            if (user == null) {
                request.setAttribute("errorMessage", "User not found.");
                RequestDispatcher view = request.getRequestDispatcher(LIST_USER);
                view.forward(request, response);
                return;
            }
            converter.userRegistrationModelToEntity(user, userModel);
            userRegistrationService.update(user);
        }
        List<UserRegistration> userList = userRegistrationService.findAll();
        request.setAttribute("userRegistrations", userList);

        RequestDispatcher view = request.getRequestDispatcher(LIST_USER);
        view.forward(request, response);
    }
}
