package glaznyov.lr45.model;

public class UserRegistrationModel {
    private Integer id;
    private String registrationDate;
    private String login;
    private String password;
    private String passwordConfirmation;
    private String lastName;
    private String firstName;
    private String email;
    private String icq;
    private String gender;
    private Boolean newsletterSubscription;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIcq() {
        return icq;
    }

    public void setIcq(String icq) {
        this.icq = icq;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Boolean getNewsletterSubscription() {
        return newsletterSubscription;
    }

    public void setNewsletterSubscription(Boolean newsletterSubscription) {
        this.newsletterSubscription = newsletterSubscription;
    }

    public UserRegistrationModel(Integer id, String registrationDate, String login, String password, String passwordConfirmation, String lastName, String firstName, String email, String icq, String gender, Boolean newsletterSubscription) {
        this.id = id;
        this.registrationDate = registrationDate;
        this.login = login;
        this.password = password;
        this.passwordConfirmation = passwordConfirmation;
        this.lastName = lastName;
        this.firstName = firstName;
        this.email = email;
        this.icq = icq;
        this.gender = gender;
        this.newsletterSubscription = newsletterSubscription;
    }

    public UserRegistrationModel() {

    }

    @Override
    public String toString() {
        return "UserRegistrationModel{" +
                "id=" + id +
                ", registrationDate='" + registrationDate + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", passwordConfirmation='" + passwordConfirmation + '\'' +
                ", lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", email='" + email + '\'' +
                ", icq='" + icq + '\'' +
                ", gender='" + gender + '\'' +
                ", newsletterSubscription=" + newsletterSubscription +
                '}';
    }
}
