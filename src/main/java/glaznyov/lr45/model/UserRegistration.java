package glaznyov.lr45.model;

import jakarta.persistence.*;

@Entity
@Table(name = "user_registration")
public class UserRegistration {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "registration_date", nullable = false)
    private String registrationDate;

    @Column(name = "login", nullable = false)
    private String login;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "password_confirmation", nullable = false)
    private String passwordConfirmation;

    @Column(name = "last_name", nullable = true)
    private String lastName;

    @Column(name = "first_name", nullable = true)
    private String firstName;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "icq", nullable = true)
    private String icq;

    @Column(name = "gender", nullable = true)
    private String gender;

    @Column(name = "newsletter_subscription", nullable = true)
    private Boolean newsletterSubscription;

    public UserRegistration(String registrationDate, String login, String password, String passwordConfirmation, String lastName, String firstName, String email, String icq, String gender, Boolean newsletterSubscription) {
        this.registrationDate = registrationDate;
        this.login = login;
        this.password = password;
        this.passwordConfirmation = passwordConfirmation;
        this.lastName = lastName;
        this.firstName = firstName;
        this.email = email;
        this.icq = icq;
        this.gender = gender;
        this.newsletterSubscription = newsletterSubscription;
    }

    public UserRegistration() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIcq() {
        return icq;
    }

    public void setIcq(String icq) {
        this.icq = icq;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Boolean getNewsletterSubscription() {
        return newsletterSubscription;
    }

    public void setNewsletterSubscription(Boolean newsletterSubscription) {
        this.newsletterSubscription = newsletterSubscription;
    }

    @Override
    public String toString() {
        return "UserRegistration{" +
                "id=" + id +
                ", registrationDate='" + registrationDate + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", passwordConfirmation='" + passwordConfirmation + '\'' +
                ", lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", email='" + email + '\'' +
                ", icq='" + icq + '\'' +
                ", gender='" + gender + '\'' +
                ", newsletterSubscription=" + newsletterSubscription +
                '}';
    }
}
