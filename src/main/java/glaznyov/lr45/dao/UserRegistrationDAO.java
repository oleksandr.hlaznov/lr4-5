package glaznyov.lr45.dao;

import glaznyov.lr45.model.UserRegistration;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class UserRegistrationDAO implements UserRegistrationDAOInterface<UserRegistration, Integer> {
    private Session currentSession;
    private Transaction currentTransaction;

    public UserRegistrationDAO() {
    }

    public Session openCurrentSession() {
        currentSession = getSessionFactory().openSession();
        return currentSession;
    }

    public Session openCurrentSessionWithTransaction() {
        currentSession = getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();
        return currentSession;
    }

    public void closeCurrentSession() {
        currentSession.close();
    }

    public void closeCurrentSessionWithTransaction() {
        currentTransaction.commit();
        currentSession.close();
    }

    private SessionFactory getSessionFactory() {
        Configuration configuration = new Configuration().configure("META-INF/hibernate.cfg.xml");
        configuration.addAnnotatedClass(UserRegistration.class);
        StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
        return configuration.buildSessionFactory(builder.build());
    }

    public Session getCurrentSession() {
        return currentSession;
    }

    public void setCurrentSession(Session currentSession) {
        this.currentSession = currentSession;
    }

    public void setCurrentTransaction(Transaction currentTransaction) {
        this.currentTransaction = currentTransaction;
    }

    @Override
    public void persist(UserRegistration entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void update(UserRegistration entity) {
        getCurrentSession().update(entity);
    }

    @Override
    public UserRegistration findById(Integer id) {
        UserRegistration userRegistration = (UserRegistration) getCurrentSession().get(UserRegistration.class, id);
        return userRegistration;
    }

    @Override
    public void delete(UserRegistration entity) {
        getCurrentSession().delete(entity);
    }

    @Override
    public List<UserRegistration> findAll() {
        List<UserRegistration> userRegistrations = (List<UserRegistration>) getCurrentSession().createQuery("from UserRegistration").list();
        return userRegistrations;
    }

    @Override
    public void deleteAll() {
        List<UserRegistration> entityList = findAll();
        for (UserRegistration entity : entityList) {
            delete(entity);
        }
    }
}
